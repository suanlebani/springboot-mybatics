package com.example.demo;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/")
@RestController
public class Hello1Controller {

    @RequestMapping("/hello1")
    public String hello() {
        return "index";
    }
}
