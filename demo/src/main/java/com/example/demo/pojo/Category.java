package com.example.demo.pojo;

import java.util.List;

public class Category {
    private Integer id;

    private String name;

    private List<Product> products;

    private List<List<Product>> listProductsByRow;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<List<Product>> getListProductsByRow() {
        return listProductsByRow;
    }

    public void setListProductsByRow(List<List<Product>> listProductsByRow) {
        this.listProductsByRow = listProductsByRow;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }
}