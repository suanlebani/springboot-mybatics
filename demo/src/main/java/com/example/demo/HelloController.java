package com.example.demo;


import com.example.demo.pojo.Category;
import com.example.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/")
@Controller
public class HelloController {

    @Autowired
    CategoryService categoryService;

    @RequestMapping("/hello")
    public String hello() {
        return "index";
    }

    @RequestMapping("/modifyCategory")

    public void modifyCategory() {
        Category category = new Category();
        category.setName("精灵");
//        List<Category> categoryList=categoryService.list();
        categoryService.add(category);
    }
}
