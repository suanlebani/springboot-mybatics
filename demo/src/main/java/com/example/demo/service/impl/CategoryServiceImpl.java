package com.example.demo.service.impl;


import com.example.demo.mapper.CategoryMapper;
import com.example.demo.pojo.Category;
import com.example.demo.pojo.CategoryExample;
import com.example.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
@Autowired
CategoryMapper categoryMapper;
    @Override
    public List<Category> list() {
        CategoryExample categoryExample=new CategoryExample();
        categoryExample.setOrderByClause("id desc");
        return categoryMapper.selectByExample(categoryExample);
    }

    @Override
    public void add(Category category) {
categoryMapper.insert(category);
    }

    @Override
    public void delete(Category c) {
categoryMapper.deleteByPrimaryKey(c.getId());
    }

    @Override
    public void update(Category c) {
categoryMapper.updateByPrimaryKey(c);
    }

    @Override
    public Category get(int id) {
        return categoryMapper.selectByPrimaryKey(id);
    }

    @Override
    public int getCount() {
        return categoryMapper.getCount();
    }
}
