package com.example.demo.service;



import com.example.demo.pojo.Category;

import java.util.List;

public interface CategoryService {
    List<Category> list();
    void add(Category category);

    void delete(Category c);

    void update(Category c);

    Category get(int id);

    int getCount();
}
